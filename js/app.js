const nsGameOfLifeApp = {
    configs: {
        numSaveStates: 5,
        randomlySeedGrid: true,
        isWorldWrapped: true,
        imageFileName: 'GameOfLifeSnapshot',
        imageFileExtension: '.png',
        colorGridLines: "#FF0000",
        colorCells: {
            alive: "black",
            dead: "white",
        },
        resolution: 10,
        width: 750,
        height: 750,
    },
    globals: {
        canvas: null,
        ctx: null,
        grid: null,
        gridSaveStates: null,
        domElements: {
            idClearGridButton: "ClearGridButton",
            idSaveImageButton: "SaveImageButton",
            idSaveAndLoadStateButtons: "SaveStateButtons",
            idSaveStateButtonPrefix: "SaveStateButton",
            valueSaveStateButtonPrefix: "Save State ",
            idLoadStateButtonPrefix: "LoadStateButton",
            valueLoadStateButtonPrefix: "Load State ",
            idCanvas: "canvas",
            idImageNameInput: "ImageNameInput",
            idNextGenButton: "NextGenButton",
        },
    },
    ui: {
        makeSaveStateButtons: function (c) {
            for (let i = 1; i <= c; i++) {
                const buttonSaveState = document.createElement("button");
                buttonSaveState.id = nsGameOfLifeApp.globals.domElements.idSaveStateButtonPrefix + i.toString();
                buttonSaveState.onclick = function () {
                    nsGameOfLifeApp.ui.saveState(i);
                }
                buttonSaveState.textContent = nsGameOfLifeApp.globals.domElements.valueSaveStateButtonPrefix + i.toString();

                const buttonLoadState = document.createElement("button");
                buttonLoadState.id = nsGameOfLifeApp.globals.domElements.idLoadStateButtonPrefix + i.toString();
                buttonLoadState.onclick = function () {
                    nsGameOfLifeApp.ui.loadState(i);
                }
                buttonLoadState.style.visibility = "hidden";
                buttonLoadState.textContent = nsGameOfLifeApp.globals.domElements.valueLoadStateButtonPrefix + i.toString();

                const breakElement = document.createElement("br");

                document.getElementById(nsGameOfLifeApp.globals.domElements.idSaveAndLoadStateButtons)
                    .appendChild(buttonSaveState);
                document.getElementById(nsGameOfLifeApp.globals.domElements.idSaveAndLoadStateButtons)
                    .appendChild(buttonLoadState);
                document.getElementById(nsGameOfLifeApp.globals.domElements.idSaveAndLoadStateButtons)
                    .appendChild(breakElement);
            }
        },

        startPainting: function (event) {
            const sketch = nsGameOfLifeApp.ui.sketch;
            const getPosition = nsGameOfLifeApp.ui.getPosition;

            sketch(getPosition(event))
        },

        getPosition: function (el) {
            return {
                x: el.clientX - nsGameOfLifeApp.globals.canvas.offsetLeft,
                y: el.clientY - nsGameOfLifeApp.globals.canvas.offsetTop,
            };
        },

        sketch: function (cursorCoordinates) {
            const Coordinates = nsGameOfLifeApp.gameLogic.Coordinates;
            const getAllSisterCoordinatesOfCell = nsGameOfLifeApp.gameLogic.getAllSisterCoordinatesOfCell;
            const render = nsGameOfLifeApp.ui.render;
            const colorCells = nsGameOfLifeApp.configs.colorCells;
            const colorGridLines = nsGameOfLifeApp.configs.colorGridLines;
            const resolution = nsGameOfLifeApp.configs.resolution;
            const isWorldWrapped = nsGameOfLifeApp.configs.isWorldWrapped;
            const COLS = nsGameOfLifeApp.globals.canvas.width / resolution;
            const ROWS = nsGameOfLifeApp.globals.canvas.height / resolution;

            let grid = nsGameOfLifeApp.globals.grid;

            const cellCoordinates = new Coordinates(
                Math.floor(cursorCoordinates.x / resolution),
                Math.floor(cursorCoordinates.y / resolution),
                false, // A wrapped universe is immaterial when it comes to the act of clicking on a screen.
                COLS,
                ROWS
            );
            if (cellCoordinates.isValidCoordinates()) {
                const positionOfCell = grid[cellCoordinates.x][cellCoordinates.y].position;
                cellCoordinates.isWorldWrapped = isWorldWrapped; // But at this point, a wrapped universe matters.
                const allSisterCoordinates = getAllSisterCoordinatesOfCell(positionOfCell, cellCoordinates);
                const isAlive = grid[allSisterCoordinates[0].x][allSisterCoordinates[0].y].isAlive;
                for (let i = 0; i < allSisterCoordinates.length; i++) {
                    console.assert(grid[allSisterCoordinates[i].x][allSisterCoordinates[i].y].isAlive === isAlive);
                    if (grid[allSisterCoordinates[i].x][allSisterCoordinates[i].y].isAlive === isAlive)
                        grid[allSisterCoordinates[i].x][allSisterCoordinates[i].y].isAlive = !grid[allSisterCoordinates[i].x][allSisterCoordinates[i].y].isAlive;
                }
                render(grid, colorCells, colorGridLines);

                nsGameOfLifeApp.globals.grid = grid;
            }
        },

        /**
         *
         * @param {Array<Array<Cell>>} grid
         * @param {{}} colorCells
         * @param {string} colorGridLines
         */
        render: function (grid, colorCells, colorGridLines) {
            const ctx = nsGameOfLifeApp.globals.ctx;
            const resolution = nsGameOfLifeApp.configs.resolution;
            const drawGridLines = nsGameOfLifeApp.ui.drawGridLines;

            for (let col = 0; col < grid.length; col++) {
                for (let row = 0; row < grid[col].length; row++) {
                    ctx.fillStyle = grid[col][row].isAlive ? colorCells.alive : colorCells.dead;
                    ctx.fillRect(col * resolution, row * resolution, resolution, resolution);
                }
            }

            nsGameOfLifeApp.globals.ctx = ctx;

            drawGridLines(grid, colorGridLines)

            document.getElementById(nsGameOfLifeApp.globals.domElements.idCanvas).focus();
        },

        /**
         * @param {Array<Array<Cell>>} grid
         * @param {string} colorGridLines
         */
        drawGridLines: function (grid, colorGridLines) {
            const ctx = nsGameOfLifeApp.globals.ctx;
            const resolution = nsGameOfLifeApp.configs.resolution;

            for (let col = 0; col < grid.length; col++) {
                for (let row = 0; row < grid[col].length; row++) {
                    const cell = grid[col][row];

                    ctx.strokeStyle = colorGridLines;
                    if (cell.isMini) {
                        ctx.beginPath();
                        ctx.rect(col * resolution, row * resolution, resolution, resolution);
                        ctx.stroke();
                    } else if (cell.position === 1) {
                        ctx.moveTo(col * resolution, (row + 1) * resolution);
                        ctx.lineTo(col * resolution, row * resolution);
                        ctx.lineTo((col + 1) * resolution, row * resolution);
                        ctx.stroke();
                    } else if (cell.position === 2) {
                        ctx.moveTo(col * resolution, row * resolution);
                        ctx.lineTo((col + 1) * resolution, row * resolution);
                        ctx.lineTo((col + 1) * resolution, (row + 1) * resolution);
                        ctx.stroke();
                    } else if (cell.position === 3) {
                        ctx.moveTo(col * resolution, row * resolution);
                        ctx.lineTo(col * resolution, (row + 1) * resolution);
                        ctx.lineTo((col + 1) * resolution, (row + 1) * resolution);
                        ctx.stroke();
                    } else if (cell.position === 4) {
                        ctx.moveTo(col * resolution, (row + 1) * resolution);
                        ctx.lineTo((col + 1) * resolution, (row + 1) * resolution);
                        ctx.lineTo((col + 1) * resolution, row * resolution);
                        ctx.stroke();
                    }
                    ctx.moveTo((col + 0.5) * resolution, (row + 0.5) * resolution);

                    // ctx.fillStyle = "#0000FF";
                    // ctx.fillText(cell.position.toString() + " (" + cell.coordinates.x.toString() + "," + cell.coordinates.y.toString() + ")", (col + 0.3) * resolution, (row + 0.5) * resolution);
                }
            }

            nsGameOfLifeApp.globals.ctx = ctx;
        },

        /*async*/
        update: function () {
            // TODO: Introduce another button in the HTML to invoke this directly.
            const grid = nsGameOfLifeApp.globals.grid;
            const colorCells = nsGameOfLifeApp.configs.colorCells;
            const colorGridLines = nsGameOfLifeApp.configs.colorGridLines;
            const nextGen = nsGameOfLifeApp.gameLogic.nextGen;
            const render = nsGameOfLifeApp.ui.render;

            // await new Promise(r => setTimeout(r, 2000));
            const gridNextGen = nextGen(grid);
            render(gridNextGen, colorCells, colorGridLines);
            // requestAnimationFrame(update);

            nsGameOfLifeApp.globals.grid = gridNextGen;
        },

        /**
         * clearGrid
         */
        clearGrid: function () {
            const colorCells = nsGameOfLifeApp.configs.colorCells;
            const colorGridLines = nsGameOfLifeApp.configs.colorGridLines;
            const render = nsGameOfLifeApp.ui.render;
            const buildGrid = nsGameOfLifeApp.gameLogic.buildGrid;

            const grid = buildGrid(false);
            document.getElementById(nsGameOfLifeApp.globals.domElements.idClearGridButton).blur();
            document.getElementById(nsGameOfLifeApp.globals.domElements.idCanvas).focus();
            render(grid, colorCells, colorGridLines);

            nsGameOfLifeApp.globals.grid = grid;
        },

        /**
         *
         * @param {int} i
         */
        loadState: function (i) {
            const render = nsGameOfLifeApp.ui.render;
            const colorCells = nsGameOfLifeApp.configs.colorCells;
            const colorGridLines = nsGameOfLifeApp.configs.colorGridLines;

            const grid = JSON.parse(JSON.stringify(nsGameOfLifeApp.globals.gridSaveStates[i]));
            document.getElementById(nsGameOfLifeApp.globals.domElements.idLoadStateButtonPrefix + i.toString()).blur();
            document.getElementById(nsGameOfLifeApp.globals.domElements.idCanvas).focus();
            render(grid, colorCells, colorGridLines);

            nsGameOfLifeApp.globals.grid = grid;
        },

        saveState: function (i) {
            nsGameOfLifeApp.globals.gridSaveStates[i] = JSON.parse(JSON.stringify(nsGameOfLifeApp.globals.grid));
            document.getElementById(nsGameOfLifeApp.globals.domElements.idLoadStateButtonPrefix + i.toString()).style.visibility = 'visible';
            document.getElementById(nsGameOfLifeApp.globals.domElements.idSaveStateButtonPrefix + i.toString()).blur();
            document.getElementById(nsGameOfLifeApp.globals.domElements.idCanvas).focus();
        },

        saveImage: function () {
            const link = document.createElement('a');
            const imageName = nsGameOfLifeApp.configs.imageFileName + nsGameOfLifeApp.configs.imageFileExtension;
            const canvas = nsGameOfLifeApp.globals.canvas;

            link.setAttribute('download', imageName);
            link.setAttribute('href', canvas.toDataURL("image/png").replace("image/png", "image/octet-stream"));
            link.click();

            document.getElementById(nsGameOfLifeApp.globals.domElements.idSaveImageButton).blur();
            document.getElementById(nsGameOfLifeApp.globals.domElements.idCanvas).focus();
        },

        updateImageFileName: function () {
            nsGameOfLifeApp.configs.imageFileName =
                document.getElementById(nsGameOfLifeApp.globals.domElements.idImageNameInput).value;
        },
    },

    gameLogic: {
        Coordinates: class {
            /**
             * @param {int} x
             * @param {int} y
             * @param isWorldWrapped
             * @param cols
             * @param rows
             */
            constructor(x, y, isWorldWrapped, cols, rows) {
                this.x = x;
                this.y = y;
                this.isWorldWrapped = isWorldWrapped;
                this.cols = cols;
                this.rows = rows;
            }

            /**
             *
             * @param {this} coordinates
             * @param {int} xDiff
             * @param {int} yDiff
             */
            static cloneCoordinates(coordinates, xDiff = 0, yDiff = 0) {
                return new nsGameOfLifeApp.gameLogic.Coordinates(
                    coordinates.x + xDiff,
                    coordinates.y + yDiff,
                    coordinates.isWorldWrapped,
                    coordinates.cols,
                    coordinates.rows
                );
            }

            isValidCoordinates() {
                if (this.isWorldWrapped) {
                    return !(this === null);
                } else {
                    // const COLS = canvas.width / resolution;
                    // const ROWS = canvas.height / resolution;
                    return !(
                        this === null
                        || this.x < 0 || this.x >= this.cols
                        || this.y < 0 || this.y >= this.rows
                    );
                }
            }
        },

        Cell: class {
            /**
             * @param {boolean} isAlive
             * @param {boolean} isMini
             * @param {nsGameOfLifeApp.gameLogic.Coordinates} coordinates
             * @param {int} position
             */
            constructor(isAlive, isMini, coordinates, position) {
                this.isMini = isMini;
                this.isAlive = isAlive;
                this.coordinates = coordinates;
                this.position = position;

                /**
                 * Position identifiers for mega-cells:
                 * _______
                 * |1 |2 |
                 * -------
                 * |3 |4 |
                 * -------
                 *
                 * And the mini-cell's position is 0.
                 */
                if (!isMini) {
                    console.assert(position !== 0, position);
                } else {
                    console.assert(position === 0, position);
                }

            }
        },

        /**
         * normalizeCoordinates - Wraps around under/overflowing coordinates in a wrapped world.
         * @param {nsGameOfLifeApp.gameLogic.Coordinates} coordinates
         * @returns {nsGameOfLifeApp.gameLogic.Coordinates}
         */
        normalizeCoordinates: function (coordinates) {
            const Coordinates = nsGameOfLifeApp.gameLogic.Coordinates;

            let normalizedCoordinates = Coordinates.cloneCoordinates(coordinates);
            if (normalizedCoordinates.isWorldWrapped) {
                if (normalizedCoordinates.x < 0) {
                    normalizedCoordinates.x = normalizedCoordinates.x + normalizedCoordinates.cols;
                } else if (normalizedCoordinates.x >= normalizedCoordinates.cols) {
                    normalizedCoordinates.x = normalizedCoordinates.x - normalizedCoordinates.cols;
                }

                if (normalizedCoordinates.y < 0) {
                    normalizedCoordinates.y = normalizedCoordinates.y + normalizedCoordinates.rows;
                } else if (normalizedCoordinates.y >= normalizedCoordinates.rows) {
                    normalizedCoordinates.y = normalizedCoordinates.y - normalizedCoordinates.rows;
                }
            }

            return normalizedCoordinates;
        },

        /**
         *
         * @param {int} position
         * @param {nsGameOfLifeApp.gameLogic.Coordinates} coordinates
         * @returns {nsGameOfLifeApp.gameLogic.Coordinates}
         */
        getIdentifierCellCoordinates: function (position, coordinates) {
            const Coordinates = nsGameOfLifeApp.gameLogic.Coordinates;
            const normalizeCoordinates = nsGameOfLifeApp.gameLogic.normalizeCoordinates;

            let coordinates_idCell;
            if (position === 0 || position === 1) {
                coordinates_idCell = Coordinates.cloneCoordinates(coordinates);
            } else if (position === 2) {
                coordinates_idCell = Coordinates.cloneCoordinates(coordinates, -1, 0);
            } else if (position === 3) {
                coordinates_idCell = Coordinates.cloneCoordinates(coordinates, 0, -1);
            } else if (position === 4) {
                coordinates_idCell = Coordinates.cloneCoordinates(coordinates, -1, -1);
            }

            coordinates_idCell = normalizeCoordinates(coordinates_idCell);

            return coordinates_idCell;
        },

        /**
         *
         * @param {int} position
         * @param {nsGameOfLifeApp.gameLogic.Coordinates} coordinates
         */
        getBestPossibleIdentifierCellCoordinates: function (position, coordinates) {
            const Coordinates = nsGameOfLifeApp.gameLogic.Coordinates;

            console.assert(coordinates.isValidCoordinates());

            let coordinates_idCell;
            if (position === 0 || position === 1) {
                coordinates_idCell = Coordinates.cloneCoordinates(coordinates);
            } else if (position === 2) {
                const pos1Coordinates = Coordinates.cloneCoordinates(coordinates, -1, 0);
                if (pos1Coordinates.isValidCoordinates()) {
                    coordinates_idCell = pos1Coordinates;
                } else {
                    // return self
                    coordinates_idCell = Coordinates.cloneCoordinates(coordinates);
                }
            } else if (position === 3) {
                const pos1Coordinates = Coordinates.cloneCoordinates(coordinates, 0, -1);
                const pos2Coordinates = Coordinates.cloneCoordinates(coordinates, +1, -1);
                if (pos1Coordinates.isValidCoordinates()) {
                    coordinates_idCell = pos1Coordinates;
                } else if (pos2Coordinates.isValidCoordinates()) {
                    coordinates_idCell = pos2Coordinates;
                } else {
                    // return self
                    coordinates_idCell = Coordinates.cloneCoordinates(coordinates);
                }
            } else if (position === 4) {
                const pos1Coordinates = Coordinates.cloneCoordinates(coordinates, -1, -1);
                const pos2Coordinates = Coordinates.cloneCoordinates(coordinates, 0, -1);
                const pos3Coordinates = Coordinates.cloneCoordinates(coordinates, -1, 0);
                if (pos1Coordinates.isValidCoordinates()) {
                    coordinates_idCell = pos1Coordinates;
                } else if (pos2Coordinates.isValidCoordinates()) {
                    coordinates_idCell = pos2Coordinates;
                } else if (pos3Coordinates.isValidCoordinates()) {
                    coordinates_idCell = pos3Coordinates;
                } else {
                    // return self
                    coordinates_idCell = Coordinates.cloneCoordinates(coordinates);
                }
            }

            return coordinates_idCell;
        },

        /**
         *
         * @param {int} position
         * @param {nsGameOfLifeApp.gameLogic.Coordinates} coordinates
         */
        getAllSisterCoordinatesOfCell: function (position, coordinates) {
            const normalizeCoordinates = nsGameOfLifeApp.gameLogic.normalizeCoordinates;
            const Coordinates = nsGameOfLifeApp.gameLogic.Coordinates;

            console.assert(coordinates.isValidCoordinates());
            let sisterCoordinates = [];
            sisterCoordinates.push(normalizeCoordinates(Coordinates.cloneCoordinates(coordinates)));
            if (position === 1) {
                const pos2Coordinates = normalizeCoordinates(Coordinates.cloneCoordinates(coordinates, +1, 0));
                const pos3Coordinates = normalizeCoordinates(Coordinates.cloneCoordinates(coordinates, 0, +1));
                const pos4Coordinates = normalizeCoordinates(Coordinates.cloneCoordinates(coordinates, +1, +1));

                if (pos2Coordinates.isValidCoordinates()) {
                    sisterCoordinates.push(pos2Coordinates);
                }
                if (pos3Coordinates.isValidCoordinates()) {
                    sisterCoordinates.push(pos3Coordinates);
                }
                if (pos4Coordinates.isValidCoordinates()) {
                    sisterCoordinates.push(pos4Coordinates);
                }
            } else if (position === 2) {
                const pos1Coordinates = normalizeCoordinates(Coordinates.cloneCoordinates(coordinates, -1, 0));
                const pos3Coordinates = normalizeCoordinates(Coordinates.cloneCoordinates(coordinates, -1, +1));
                const pos4Coordinates = normalizeCoordinates(Coordinates.cloneCoordinates(coordinates, 0, +1));

                if (pos1Coordinates.isValidCoordinates()) {
                    sisterCoordinates.push(pos1Coordinates);
                }
                if (pos3Coordinates.isValidCoordinates()) {
                    sisterCoordinates.push(pos3Coordinates);
                }
                if (pos4Coordinates.isValidCoordinates()) {
                    sisterCoordinates.push(pos4Coordinates);
                }
            } else if (position === 3) {
                const pos1Coordinates = normalizeCoordinates(Coordinates.cloneCoordinates(coordinates, 0, -1));
                const pos2Coordinates = normalizeCoordinates(Coordinates.cloneCoordinates(coordinates, +1, -1));
                const pos4Coordinates = normalizeCoordinates(Coordinates.cloneCoordinates(coordinates, +1, 0));

                if (pos1Coordinates.isValidCoordinates()) {
                    sisterCoordinates.push(pos1Coordinates);
                }
                if (pos2Coordinates.isValidCoordinates()) {
                    sisterCoordinates.push(pos2Coordinates);
                }
                if (pos4Coordinates.isValidCoordinates()) {
                    sisterCoordinates.push(pos4Coordinates);
                }
            } else if (position === 4) {
                const pos1Coordinates = normalizeCoordinates(Coordinates.cloneCoordinates(coordinates, -1, -1));
                const pos2Coordinates = normalizeCoordinates(Coordinates.cloneCoordinates(coordinates, 0, -1));
                const pos3Coordinates = normalizeCoordinates(Coordinates.cloneCoordinates(coordinates, -1, 0));

                if (pos1Coordinates.isValidCoordinates()) {
                    sisterCoordinates.push(pos1Coordinates);
                }
                if (pos2Coordinates.isValidCoordinates()) {
                    sisterCoordinates.push(pos2Coordinates);
                }
                if (pos3Coordinates.isValidCoordinates()) {
                    sisterCoordinates.push(pos3Coordinates);
                }
            }

            return sisterCoordinates;
        },

        /**
         *
         * @param {boolean} isRandomSeed
         * @returns {Array<Array<Cell>>}
         */
        buildGrid: function (isRandomSeed) {
            const COLS = nsGameOfLifeApp.globals.canvas.width / nsGameOfLifeApp.configs.resolution;
            const ROWS = nsGameOfLifeApp.globals.canvas.height / nsGameOfLifeApp.configs.resolution;
            const isWorldWrapped = nsGameOfLifeApp.configs.isWorldWrapped;
            const Coordinates = nsGameOfLifeApp.gameLogic.Coordinates;
            const getIdentifierCellCoordinates = nsGameOfLifeApp.gameLogic.getIdentifierCellCoordinates;
            const getBestPossibleIdentifierCellCoordinates = nsGameOfLifeApp.gameLogic.getBestPossibleIdentifierCellCoordinates;
            const Cell = nsGameOfLifeApp.gameLogic.Cell;

            if (isWorldWrapped) {
                const isWorldCorrectlyWrapped = (COLS % 5 === 0 && ROWS % 5 === 0);
                console.assert(isWorldCorrectlyWrapped,
                    "For a world to wrap around correctly, the dimensions (" + COLS.toString() + "," + ROWS.toString() + ") must be divisible by 5."
                );

                if (!isWorldCorrectlyWrapped) {
                    alert(
                        "For a world to wrap around correctly, the dimensions (" + COLS.toString() + "," + ROWS.toString() + ") must be divisible by 5."
                    );
                }
            }

            let grid = new Array(COLS).fill(null)
                .map(() => new Array(ROWS).fill(null));

            let currentRowStartPosition = 0;
            let position = 0;

            for (let i = 0; i < COLS; i++) {
                position = currentRowStartPosition;
                for (let j = 0; j < ROWS; j++) {
                    const coordinates = new Coordinates(i, j, isWorldWrapped, COLS, ROWS);
                    const coordinates_idCell =
                        isWorldWrapped
                            ? getIdentifierCellCoordinates(position, coordinates)
                            : getBestPossibleIdentifierCellCoordinates(position, coordinates);
                    if (!coordinates_idCell.isValidCoordinates())
                        console.assert(isWorldWrapped); // This scenario is only possible in a wrapped world.
                    const isAlive =
                        isRandomSeed ?
                            (position <= 1 || !coordinates_idCell.isValidCoordinates() || grid[coordinates_idCell.x][coordinates_idCell.y] === null
                                ? Math.random() > 0.75
                                : grid[coordinates_idCell.x][coordinates_idCell.y].isAlive)
                            : false;
                    const isMini = position === 0;

                    grid[i][j] = new Cell(isAlive, isMini, coordinates, position);

                    position = (position + 2) % 5;
                }
                currentRowStartPosition = (currentRowStartPosition + 1) % 5;
            }
            return grid;
        },

        /**
         * applyNextGenRules
         * @param {nsGameOfLifeApp.gameLogic.Cell} cell
         * @param {int} livingNeighborCount
         * @returns {boolean} isAliveInNextGen
         */
        applyNextGenRules: function (cell, livingNeighborCount) {
            let isAliveInNextGen = cell.isAlive;

            if (cell.isMini) {
                if (cell.isAlive && livingNeighborCount < 2) { // underpopulation
                    isAliveInNextGen = false;
                } else if (cell.isAlive && livingNeighborCount > 1) { // overpopulation
                    isAliveInNextGen = false;
                } else if (!cell.isAlive && livingNeighborCount >= 4 && livingNeighborCount <= 4) { // reproduction
                    isAliveInNextGen = true;
                }
            } else {
                if (cell.isAlive && livingNeighborCount < 5) { // underpopulation
                    isAliveInNextGen = false;
                } else if (cell.isAlive && livingNeighborCount > 7) { // overpopulation
                    isAliveInNextGen = false;
                } else if (!cell.isAlive && (livingNeighborCount >= 3 && livingNeighborCount <= 4)) { // reproduction
                    isAliveInNextGen = true;
                }
            }

            return isAliveInNextGen;
        },

        /**
         * nextGen
         * @param {Array<Array<Cell>>} grid
         * @returns {Array<Array<Cell>>} nextGen
         */
        nextGen: function (grid) {
            const getIdentifierCellCoordinates = nsGameOfLifeApp.gameLogic.getIdentifierCellCoordinates;
            const applyNextGenRules = nsGameOfLifeApp.gameLogic.applyNextGenRules;
            const normalizeCoordinates = nsGameOfLifeApp.gameLogic.normalizeCoordinates;
            const Coordinates = nsGameOfLifeApp.gameLogic.Coordinates;

            const nextGen = JSON.parse(JSON.stringify(grid));

            for (let col = 0; col < grid.length; col++) {
                for (let row = 0; row < grid[col].length; row++) {
                    const cell = grid[col][row];
                    let livingNeighborCount = 0;

                    if (cell.isMini) {
                        for (let i = -1; i < 2; i++) {
                            for (let j = -1; j < 2; j++) {
                                if (i === 0 && j === 0) {
                                    continue;
                                }

                                const currentNeighborCoordinates = normalizeCoordinates(Coordinates.cloneCoordinates(cell.coordinates, i, j));

                                if (currentNeighborCoordinates.isValidCoordinates()) {
                                    const [x_cell, y_cell] = [currentNeighborCoordinates.x, currentNeighborCoordinates.y];
                                    const currentNeighbour = grid[x_cell][y_cell];
                                    livingNeighborCount += currentNeighbour.isAlive ? 1 : 0;
                                }
                            }
                        }
                    } else {
                        const coordinates_idCell = getIdentifierCellCoordinates(cell.position, cell.coordinates);
                        for (let i = -1; i < 3; i++) {
                            for (let j = -1; j < 3; j++) {
                                if (i === 0 && j === 0)
                                    continue;
                                if (i === 0 && j === 1)
                                    continue;
                                if (i === 1 && j === 0)
                                    continue;
                                if (i === 1 && j === 1)
                                    continue;

                                const currentNeighborCoordinates = normalizeCoordinates(Coordinates.cloneCoordinates(coordinates_idCell, i, j));

                                if (currentNeighborCoordinates.isValidCoordinates()) {
                                    const [x_cell, y_cell] = [currentNeighborCoordinates.x, currentNeighborCoordinates.y];
                                    const currentNeighbour = grid[x_cell][y_cell];
                                    livingNeighborCount += currentNeighbour.isAlive ? 1 : 0;
                                }
                            }
                        }
                    }

                    nextGen[col][row].isAlive = applyNextGenRules(cell, livingNeighborCount);
                }
            }
            return nextGen;
        },
    },

    init: function () {
        // wait for the content of the window element
        // to load, then performs the operations.
        // This is considered best practice.
        window.addEventListener('load', () => {
            document.addEventListener('mousedown', nsGameOfLifeApp.ui.startPainting);
        });

        nsGameOfLifeApp.globals.canvas = document.querySelector('canvas');
        nsGameOfLifeApp.globals.ctx = nsGameOfLifeApp.globals.canvas.getContext('2d');
        nsGameOfLifeApp.globals.canvas.height = nsGameOfLifeApp.configs.height;
        nsGameOfLifeApp.globals.canvas.width = nsGameOfLifeApp.configs.width;
        nsGameOfLifeApp.ui.makeSaveStateButtons(nsGameOfLifeApp.configs.numSaveStates);

        document.getElementById(nsGameOfLifeApp.globals.domElements.idClearGridButton)
            .onclick = nsGameOfLifeApp.ui.clearGrid;

        document.getElementById(nsGameOfLifeApp.globals.domElements.idSaveImageButton)
            .onclick = nsGameOfLifeApp.ui.saveImage;

        document.getElementById(nsGameOfLifeApp.globals.domElements.idImageNameInput)
            .value = nsGameOfLifeApp.configs.imageFileName;
        document.getElementById(nsGameOfLifeApp.globals.domElements.idImageNameInput)
            .onchange = nsGameOfLifeApp.ui.updateImageFileName;

        document.body.onkeyup = function (e) {
            if (e.keyCode === 32) {
                nsGameOfLifeApp.ui.update();
            }
        };

        document.getElementById(nsGameOfLifeApp.globals.domElements.idNextGenButton)
            .onclick = function () {

            nsGameOfLifeApp.ui.update();

            document.getElementById(nsGameOfLifeApp.globals.domElements.idNextGenButton).blur();
            document.getElementById(nsGameOfLifeApp.globals.domElements.idCanvas).focus();
        };

        nsGameOfLifeApp.globals.grid = nsGameOfLifeApp.gameLogic.buildGrid(nsGameOfLifeApp.configs.randomlySeedGrid);

        nsGameOfLifeApp.globals.gridSaveStates = [];
        for (let i = 0; i < nsGameOfLifeApp.configs.numSaveStates; i++) {
            nsGameOfLifeApp.globals.gridSaveStates[i] = JSON.parse(JSON.stringify(nsGameOfLifeApp.globals.grid))
        }

        nsGameOfLifeApp.ui.render(nsGameOfLifeApp.globals.grid, nsGameOfLifeApp.configs.colorCells, nsGameOfLifeApp.configs.colorGridLines);
        // requestAnimationFrame(update);
    },
}

nsGameOfLifeApp.init();